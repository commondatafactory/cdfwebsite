set -x
set -u
set -e

sudo docker-compose stop web
sudo docker-compose rm web
sudo docker-compose build web
sudo docker-compose up -d web
