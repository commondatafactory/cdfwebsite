# commondatafactory.nl
This repository contains the static website [nlx.io](https://www.commondatafactory.nl).

## Developing

To have a live server running at http://localhost:8080. Use Docker with:

    docker-compose build
    docker-compose up

This builds a new nginx container with the static files, listening on port 80.
