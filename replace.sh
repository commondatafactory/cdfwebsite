#!/bin/sh

set -x

echo "Converting https://commondatafactory.nl to ${BASE_URL}"
sed -i -e"s;https://commondatafactory.nl/;${BASE_URL};" /usr/share/nginx/html/index.html
