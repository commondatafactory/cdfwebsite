window._env_ = {
  TEGOLA_URL: "https://commondatafactory.nl/t/",
  MAPSERVER_URL: "https://commondatafactory.nl/maps/",
  Gasverbruik_URL: "https://geodata.nationaalgeoregister.nl/cbsenergieleveringen/wfs?typename=cbsenergieleveringen:aardgas_buurt_woningen_2014",
  Gezin_URL: "https://geodata.nationaalgeoregister.nl/wijkenbuurten2018/wfs?TYPENAME=wijkenbuurten2018:cbs_buurten_2018"
}