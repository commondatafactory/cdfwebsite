// ==========================================================
// Authors: Kees Kraan, Oscar van Capel
// Organisation: Vereniging Nederlandse Gemeenten (VNG Realisatie)
// Last change: 24-12-2019
// File: map.js
// Explanation:
//      This file defines how to fill index.html.
//      This website shows a map with data of the Netherlands and will be used by municipalities to make informed decisions about the "Transitievisie Warmte".
//      In this project, municipalities have to make a plan to get rid of gas connections (decided in the Paris Agreement of 12-12-2015).
//      The data that is being shown depends on the wishes of the municipalities.
//===========================================================

//Declare and intitialize all global variables, such as layers (used to fill the map), dictionaries and visualisation styles
//The first few are used to create the map and its layers using leaflet.
var mymap, baseLayer;
var gasverbruikLayer = L.geoJson();
var gezinsLayer = L.geoJson();
var Scenario1 = L.geoJson();
var overlayMaps, baseMaps;
// The following few variables are connected to the creation of the sliders
var std_devs = {};
var sliders = {};
var switchVisible = false;
var x = d3
    .scaleLinear()
    .range([0, 100]);
// info enables that a follow-up action can happen concerning a specific datapoint, when that datapoint is clicked on
var info = L.control();
// legend reserves a space to add a legend to the map (should become interactive, depending on the selection of layers)
var legend = L.control({position: 'bottomright'});

//url_parameters is the part of the wfs-urls that is equal for all wfs's.
var url_parameters = '&request=GetFeature&service=WFS&outputFormat=json&srsName=EPSG:4326&version=2.0.0';
//variable_selection is a dictionary of names of the fields that can be visualized, with behind it a boolean to indicate whether it is actually shown or not
var variable_selection = {'Gemiddelde_aardgaslevering_woningen':false, 'Aantal_toegewezen_leveringsadressen_aardgas_woningen':false, 'gemiddelde_huishoudsgrootte':false};
// variable_list is going to abundant (when wip is finished)
var variable_list = [];
// layerDictionary is the translation between the name for layers used in this code and the name shown on the dashboard
var layerDictionary = {"Gasverbruik per buurt" : gasverbruikLayer, "Gezinssamenstelling" : gezinsLayer};
// slider_style defines the style used by the sliders
var slider_style = {
    fillColor: 'green',
    fillOpacity: 0.5,
    weight: 0.7,
    opacity: 1,
    color: 'blue',
};
// out_style is the style that datapoints get when it is outside the selection of the sliders
var out_style = {
    fillOpacity: 0,
//    fillColor: '#000000',
    weight: 0.2,
    opacity: 1,
    color: 'white'
};

// getColor is a function that assigns a color to a datapoint depending on its value (colorscale: red-yellow-green)
function getColor(value) {
    return value > 6000 ? "#ff4144" :
           value > 5000 ? "#fb693e" :
           value > 4000 ? "#f69039" :
           value > 3000 ? "#efcc31" :
           value > 2000 ? "#d3cb30" :
           value > 1500 ? "#aec92f" :
           value > 1000 ? "#9bc82e" :
           value > 500  ? "#6cc52d" :
           value > 0    ? "#2bc12b" :
           value <= 0   ? "#111111" :
           "#000000";
}

// the styling()-function returns the defined styling for datapoints that are within the range of the selection of the sliders
function styling(features) {
    return {
        fillColor: getColor(features.properties["Gemiddelde_aardgaslevering_woningen"]),
        weight: 0.3,
        opacity: 1,
        fillOpacity: 0.7
    };
}

// initMap() renders the baseLayer (a worldmap), sets the view and zoom level (Amersfoort is the center of the view)
// the values of info (possibility to click on items) and a legend are added to the map
function initMap() {
    mymap = L.map('map', {renderer: L.canvas()}).setView([52.150556, 5.345833], 8);
    baseLayer = L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(mymap);
    info.addTo(mymap);
    legend.addTo(mymap);
}

// initLayers() categorizes the maps into different layers, and adds them to the map, enabling their selection (and thus view)
function initLayers(ev) {
    //baseMaps is the underground (only 1 at a time can be selected)
    //overlayMaps is the additional information on top of a baseMap (multiple at once can be selected)
    baseMaps = {
        "Ondergrond": baseLayer
    };
    //use .addTo(mymap) to make the layer active at start
    overlayMaps = layerDictionary;
    control = L.control.layers(baseMaps, overlayMaps).addTo(mymap);
}


// TODO: create function that adds layers automatically based on provided WFS url's (not yet working)
// addLayer() automates the addition of layers, based only the provided url's
function addLayer(url) {
    //add geoJson layers to the map
    var layerName = L.geoJson();
    url = url + url_parameters;
    return $.getJSON(url, function(data) {
        $.each(data.features, function(index, geometry) {
            layerName.addData(geometry);
        });
    });
}

// REDUNDANT FUNCTION!!!! (when fillcolor is necessary again, this function adds value, otherwise it doesn't)
//function set_filter_color(e) {
//    return slider_style;
//    // style.fillColor = color_map[e.properties.clusters];
//}

// new_filter() calls for evaluation whether datapoints of a layer are within or outside the selection of the sliders and assigns the relevant style
// TODO: add evaluation for all layers
function new_filter() {
    gasverbruikLayer.setStyle(function (object) {
        return evaluate_filters(object.properties) ? styling(object) : out_style
    });
}

// evaluate_filter() checks for every active slider whether datapoints lie within or outside the selection of the sliders
function evaluate_filters(props) {
    for (var variable_name in sliders) {
        var filter_values = sliders[variable_name].value();
        if (!(props[variable_name] >= filter_values[0] && props[variable_name] <= filter_values[1])) {
            return false;
        }
    }
    return true
}

// render_slider() returns the html-code for tablerows to the page. In these tablerows, the sliders are shown.
function render_slider(columnName) {
    var element = document.createElement("tr");
    element.innerHTML = '<td class="text-wrap">' + columnName.replace(/_/g, " ") + '</td>' +
        '<td  id="' + columnName + '"></td>' +
        '<td class="value-cell" ><span id="' + columnName + '-value"></span></td>';
    return element;
}

// createSlider() adds sliders on the webpage next to the map based
// The style is fixed, the values are dependent on the values of the data in the column (varName)
// It also checks for use/change of the sliders (with .on(...))
function createSlider(varName, min, max) {
    var step = (max - min) / 100;
    var slider = d3
        .sliderBottom()
        .min(min)
        .max(max)
        .step(step)
        .width(220)
        // .height(15)
        .default([min, max])
        .fill('#28a745')
        // .ticks(3)
        .displayValue(true)
        .tickValues([min, max])
        .on('onchange', val => {
            sliders[varName].tickValues(val);
            svg.call(sliders[varName]);
            new_filter();
        });

    var svg = d3.select('#' + varName)
        .append('svg')
        .attr('width', 300)
        .attr('height', 45);

    svg.append('g')
        .attr('id', varName + '-slider')
        .attr('transform', 'translate(30,10)')
        .call(slider);
    return slider
}

// onEachFeature() defines which actions to take when 1) hovering over a datapoint, 2) not hovering over a datapoint anymore, 3) when a datapoint is clicked on
function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: onClick
    });
}

// highlightFeature() happens when the mouse hovers over a specific datapoint (e) and adjust its style and updates the info
// TODO: show correct info and from multiple sources/columns
function highlightFeature(datapoint) {
    var layer = datapoint.target;

    layer.setStyle({
        weight: 3
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }

    info.update(layer.feature.properties);
}

// resetHightlight() resets the style of a datapoint after the mouse has hovered over it
// TODO: check if removal of parameter 'e' is possible?
function resetHighlight(e) {
    L.geoJSON(baseLayer, {
        style: slider_style,
        onEachFeature: onEachFeature
    }).setStyle({weight: 1});
    info.update();
}

// onClick() takes action when a datapoint is clicked on. Action = show info relevant to this datapoint and show a marker in the sliders to show this datapoint's values
function onClick(datapoint) {
    var layer = datapoint.target;
    document.getElementById('gemeente-naam').innerText = layer.feature.properties.buurtcode;
    document.getElementById('mark-icon').innerHTML = '<svg><g transform="translate(15,15)">' +
        '<path transform="rotate(360)" d="M-5.5,-5.5v10l6,5.5l6,-5.5v-10z" class="handle" fill="#5D3A9B" stroke="#777"></path>' +
        '</g></svg>';
    /*document.getElementById('similar-results').innerHTML = '';*/
    // gmcode = id of the municipality
    gmcode = layer.feature.properties.code;
    var innerTable = '';
    // clear data from any previous selection
    [...document.getElementsByClassName('mark')].forEach(datapoint => datapoint.remove());
    // add a marker on the slider at the correct place (depends on the value of the property)
    for (var variable_name in sliders) {
        var elem = document.getElementById(variable_name + '-value');
        elem.innerHTML = ' <b>(' + math.round(layer.feature.properties[variable_name], 4) + ')</b>';
        elem.style.backgroundColor = 'white';
    }

    document.getElementById('gemeente-info').innerHTML = innerTable;

    if (switchVisible) {
        toggleSwitches()
    }
}

// toggleSwitches() makes invisible the classlist of each datapoint.
function toggleSwitches() {
    [...document.getElementsByClassName('component-switch')].forEach(datapoint => datapoint.classList.toggle('invisible'));
    switchVisible = !switchVisible;
}

// When the variable 'info' is added to the webpage, activate this function to reserve space for it on the webpage, and fill it with correct data
info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this.update();
    return this._div;
};

// method that we will use to update the 'info'-part based on feature properties passed
info.update = function (props) {
    this._div.innerHTML = (props) ? ('<b>' + props.buurtcode + '</b>') : '';
};

//When the legend is added to the map, reserve its space and fill it with the values, labels and colors
//TODO: make this function adaptive to all selected layers, not only hardcoded Gasverbruik
legend.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend'),
    grades = [0, 500, 1000, 1500, 2000, 3000, 4000, 5000],
    labels = ['<strong> Gasverbruik </strong><br>'];

    // loop through our density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        labels.push(
            '<colorBlocks style="background:' + getColor(grades[i] + 1) + '"></colorBlocks> ' +
            grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+')
        )
    }
    div.innerHTML = labels.join(' ');
    return div;
};

//initiate the map at the correct position
initMap();

//add geoJson layers to the map
url = window._env_.Gasverbruik_URL + url_parameters + '&count=1500';
$.getJSON(url, function (data) {
    gasverbruikLayer = L.geoJson(data, {data: data.features, style: styling, type: "FeatureCollection"}).addTo(gasverbruikLayer);
});

url = window._env_.Gezin_URL + url_parameters + '&count=1500';
$.getJSON(url, function (data) {
    gezinsLayer = L.geoJson(data, {data: data.features, style: styling, type: "FeatureCollection"}).addTo(gezinsLayer);
});


//var url = 'http://192.168.99.100/maps/sc_corp?typeName=ms:laag_inkomen&Count=1000' + url_parameters;
//$.getJSON(url, function (data) {
//    variable_list = Object.keys(data.features[0].properties);
//    baseLayer = {type:data.type,features:data.features};
//    render_slider();
//
//    for (var j = 0; j < variable_list.length; j++) {
//        if (variable_selection.includes(variable_list[j])) {
//            sliders[variable_list[j]] = createSlider(variable_list[j]);
//            std_devs[variable_list[j]] = math.std(baseLayer.features.map(a => a.properties[variable_list[j]]))
//        }
//    }
//});

// onOverlayAdd() is a function that becomes active when layers are selected.
// The action that then happens is the addition of relevant sliders.
function onOverlayAdd(layerInfo) {
    onOverlayAction(layerInfo, true);
}

// onOverlayRemove() is a function that becomes active when layers are deselected.
// The action that then happens is the removal of relevant sliders.
function onOverlayRemove(layerInfo) {
    onOverlayAction(layerInfo, false);
}

// onOverlayAction() searches for the column names (properties) of the layer that is (de)selected
// If a layer is deselected (add = false), its variables are set to false in the selection, and its html elements are removed
// If a layer is selected (add = true), its variables are set to true in the selection and html elements are added.
// For every variable that is true in the selection, sliders are added accordingly
function onOverlayAction(layerInfo, add){
    var layer = layerDictionary[layerInfo.name];
    var y, j;
    //This loop is necessary to get the first object of the list (not necessarily index 0)
    for (i in layer._layers) {
        if (layer._layers[i] != undefined) {
            y = i;
            break;
        }
    }
    var featsArray = Object.values(layer._layers[y]._layers);
    var featsMap = featsArray.map((object) => object.feature.properties);
    for (j in variable_selection) {
        //Check whether some variables that are deselected, exist in the properties of the layer
        //Besides check whether the variable is still being shown
        if(typeof(featsMap[0][j]) !== 'undefined' && variable_selection[j] && add == false) {
            //Remove the elements and visuals of the sliders, as well as the table rows.
            d3.select('#' + j).select("svg").remove();
            document.getElementById(j).parentNode.remove();
            variable_selection[j] = false;
        } else if (typeof(featsMap[0][j]) !== 'undefined' && ! variable_selection[j] && add == true) {
            variable_selection[j] = true;
        }
        //For all variables that should be shown, add a row in html-table and create the sliders
        if(typeof(featsMap[0][j]) !== 'undefined' && variable_selection[j]) {
            document.getElementById('sliders').appendChild(render_slider(j));
            var min = Math.min(...featsArray.map((object) => object.feature.properties[j]));
            var max = Math.max(...featsArray.map((object) => object.feature.properties[j]));
            sliders[j] = createSlider(j, min, max);
        }
    }
}
mymap.on('overlayadd', onOverlayAdd).on('overlayremove', onOverlayRemove);
initLayers();
