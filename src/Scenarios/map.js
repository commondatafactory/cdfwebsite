var mymap, geojson, basemap, meta;
var firstload = true;
var std_devs = {};
var sliders = {};
var switchVisible = false;
var normalized;
var x = d3
    .scaleLinear()
    .range([0, 220]);
var info = L.control();

var variable_selection = [
	'uitkeringsgerechtigden_percentage',
	'arbeidsongeschiktheid_percentage',
	'elekverbruik_relatief_aan_inkomen',
	'gasverbruik_relatief_aan_inkomen',
	'elektriciteitsverbruik_per_huis_gemiddeld',
	'stadsverwarming_percentage',
	'inkomen_gemiddeld_per_inwoner',
	'huishoudgrootte_gemiddeld',
	'woningwaarde_gemiddeld',
	'koopwoningen_percentage',
	'gasverbruik_per_huis_gemiddeld',
	'lagerinkomen_percentage_huishouden',
	'woningcorporatie_woningen_percentage',
	'bouwjaar_percentage_voor2000'
]

var Progressiviteit = [];
var Verbruik = [
    'elekverbruik_relatief_aan_inkomen',
    'gasverbruik_relatief_aan_inkomen',
    'elektriciteitsverbruik_per_huis_gemiddeld',
    'inkomen_gemiddeld_per_inwoner',
    'gasverbruik_per_huis_gemiddeld'
]

var Energielabel = [];

var scenario_dict = {'Progressiviteit': Progressiviteit, 'Verbruik': Verbruik, 'Energielabel': Energielabel};

var filter_style = {
    fillColor: 'green',
    fillOpacity: 0.5,
    weight: 0.7,
    opacity: 1,
    color: 'black',
};

var out_style = {
    fillColor: 'black',
    fillOpacity: 0.1,
    weight: 0.7,
    opacity: 1,
    color: 'black'
};

function populate_datalist() {
    var datalistgemeente=document.getElementById("citynames")

    for (var key in gemeentelist) {
        var opt = document.createElement("OPTION");
        opt.value = gemeentelist[key];
	opt.text = key;
        datalistgemeente.appendChild(opt);
    }
}

function initMap() {
    mymap = L.map('map').setView([52.150556, 5.345833], 8);
    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(mymap);
    populate_datalist()
}


function populateMap(){
    geojson = L.geoJSON(basemap, {
            style: set_filter_color,
            onEachFeature: onEachFeature
        }
    ).addTo(mymap);
    info.addTo(mymap);
}

function render_slider(columnName) {
    var element = document.createElement("tr");
    element.innerHTML = '<td class="text-wrap">' + columnName.replace(/_/g, " ") + '</td>' +
        '<td  id="' + columnName + '"></td>' +
        '<td class="value-cell" ><span id="' + columnName + '-value"></span></td>';
    return element;
}

function set_filter_color(e) {
    var style = filter_style;
    // style.fillColor = color_map[e.properties.clusters];
    return style
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: onClick
    });
}

function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 3
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }

    info.update(layer.feature.properties);
}

function resetHighlight(e) {
    geojson.setStyle({weight: 1});
    info.update();
}

function onClick(e) {
    var layer = e.target;
    document.getElementById('gemeente-naam').innerText = layer.feature.properties.buurtnaam;
    document.getElementById('mark-icon').innerHTML = '<svg><g transform="translate(15,15)">' +
        '<path transform="rotate(360)" d="M-5.5,-5.5v10l6,5.5l6,-5.5v-10z" class="handle" fill="#5D3A9B" stroke="#777"></path>' +
        '</g></svg>';
    /*document.getElementById('similar-results').innerHTML = '';*/
    gmcode = layer.feature.properties.code;
    var innerTable = '';
    [...document.getElementsByClassName('mark')].forEach(e => e.remove());
    for (var variable_name in sliders) {
        var elem = document.getElementById(variable_name + '-value');
        elem.innerHTML = ' <b>(' + math.round(layer.feature.properties[variable_name], 4) + ')</b>';
        elem.style.backgroundColor = 'white';
    }

    document.getElementById('gemeente-info').innerHTML = innerTable;

    if (switchVisible) {
        toggleSwitches()
    }
}

function toggleSwitches() {
    [...document.getElementsByClassName('component-switch')].forEach(e => e.classList.toggle('invisible'));
    switchVisible = !switchVisible;
}


info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this.update();
    return this._div;
};

// method that we will use to update the control based on feature properties passed
info.update = function (props) {
    this._div.innerHTML = (props) ? ('<b>' + props.buurtnaam + '</b>') : '';
};

$("#select_gemeente").click(function(){
    loadsite();
});

function createSlider(varName) {
    console.log(varName);
    var min = math.min(basemap.features.map(a => a.properties[varName]));
    var max = math.max(basemap.features.map(a => a.properties[varName]));
    var step = (max - min) / 100;

    var slider = d3
        .sliderBottom()
        .min(min)
        .max(max)
        .step(step)
        .width(220)
        // .height(15)
        .default([min, max])
        .fill('#28a745')
        // .ticks(3)
        .displayValue(true)
        .tickValues([min, max])
        .on('onchange', val => {
            sliders[varName].tickValues(val);
            svg.call(sliders[varName]);
            filter();
        });

    var svg = d3.select('#' + varName)
        .append('svg')
        .attr('width', 300)
        .attr('height', 45);

    svg.append('g')
        .attr('id', varName + '-slider')
        .attr('transform', 'translate(30,10)')
        .call(slider);

    return slider
}

function filter() {
    geojson.setStyle(function (prop) {
        return evaluate_filters(prop.properties) ? set_filter_color(prop) : out_style
    });
}

function evaluate_filters(values) {
    for (var variable_name in sliders) {
        var filter_values = sliders[variable_name].value();
        if (!(values[variable_name] >= filter_values[0] && values[variable_name] <= filter_values[1])) {
            return false;
        }
    }
    return true
}

function getGemeenteSelection(){
	var filter = $('#goption').val() || 'BU0363';
	return filter + '*';
}

function getParams() {
    var params = '';
    params = 'maps/sc_corp?service=wfs';
    params += '&version=2.0.0';
    params += '&request=GetFeature';
    params += '&typeName=ms:laag_inkomen';
    params += '&Count=400';
    params += '&outputFormat=geojson'
    params += '&srsName=epsg:4326'
    params += "&filter=<Filter><PropertyIsLike wildcard='*' singlechar='.' escape='!'><PropertyName>ms:buurtcode</PropertyName>"
    params += "<Literal>";
    params += getGemeenteSelection();
    params += "</Literal></PropertyIsLike></Filter>";

    return params
}

function loadsite() {
    var rmNode = document.getElementById('sliders');
    while (rmNode.firstChild) {
        rmNode.removeChild(rmNode.firstChild);
    }
	var url = window._env_.MAPSERVER_URL;
	console.log(url + getParams());
//	var url = window._env_.MAPSERVER_URL;
	$.getJSON(url + getParams(), function (data) {
//	    variable_list = Object.keys(data.features[0].properties);
        basemap = {type:data.type,features:data.features};
        if (firstload) {
            populateMap();
            firstload = false;
        } else {
            geojson.addData(basemap);
        }

	    for (var j = 0; j < variable_list.length; j++) {
            document.getElementById('sliders').appendChild(render_slider(variable_list[j]));
            sliders[variable_list[j]] = createSlider(variable_list[j]);
            std_devs[variable_list[j]] = math.std(basemap.features.map(a => a.properties[variable_list[j]]));
	    }
	});
}

$('.ScenarioBox').on('sumo:closed', function() {
//$('.ScenarioBox').on('change', function() {
    variable_list = [];
    $('option:checked', this).each(function() {
        for (values in scenario_dict[this.value]) {
            variable_list.push(scenario_dict[this.value][values]);
        }
    });
    variable_list = [...new Set(variable_list)];
    loadsite();
});

$(document).ready(function () {
	console.log('ready');
	$('.ScenarioBox').SumoSelect({
        placeholder:"Kies uw scenario",
        csvDispCount: 1
    });
    var Selection = $('select.ScenarioBox').SumoSelect();
    for (var key in scenario_dict) {
        Selection.sumo.add(key);
    }
    initMap();
	loadsite();
});

