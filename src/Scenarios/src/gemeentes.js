var gemeentelist = {
    "Appingedam": "BU0003"
  ,
    "Bedum": "BU0005"
  ,
    "Ten Boer": "BU0009"
  ,
    "Delfzijl": "BU0010"
  ,
    "Groningen": "BU0014"
  ,
    "Grootegast": "BU0015"
  ,
    "Haren": "BU0017"
  ,
    "Leek": "BU0022"
  ,
    "Loppersum": "BU0024"
  ,
    "Marum": "BU0025"
  ,
    "Almere": "BU0034"
  ,
    "Stadskanaal": "BU0037"
  ,
    "Veendam": "BU0047"
  ,
    "Zeewolde": "BU0050"
  ,
    "Winsum": "BU0053"
  ,
    "Zuidhorn": "BU0056"
  ,
    "Dongeradeel": "BU0058"
  ,
    "Achtkarspelen": "BU0059"
  ,
    "Ameland": "BU0060"
  ,
    "Harlingen": "BU0072"
  ,
    "Heerenveen": "BU0074"
  ,
    "Kollumerland en Nieuwkruisland": "BU0079"
  ,
    "Leeuwarden": "BU0080"
  ,
    "Ooststellingwerf": "BU0085"
  ,
    "Opsterland": "BU0086"
  ,
    "Schiermonnikoog": "BU0088"
  ,
    "Smallingerland": "BU0090"
  ,
    "Terschelling": "BU0093"
  ,
    "Vlieland": "BU0096"
  ,
    "Weststellingwerf": "BU0098"
  ,
    "Assen": "BU0106"
  ,
    "Coevorden": "BU0109"
  ,
    "Emmen": "BU0114"
  ,
    "Hoogeveen": "BU0118"
  ,
    "Meppel": "BU0119"
  ,
    "Almelo": "BU0141"
  ,
    "Borne": "BU0147"
  ,
    "Dalfsen": "BU0148"
  ,
    "Deventer": "BU0150"
  ,
    "Enschede": "BU0153"
  ,
    "Haaksbergen": "BU0158"
  ,
    "Hardenberg": "BU0160"
  ,
    "Hellendoorn": "BU0163"
  ,
    "Hengelo": "BU0164"
  ,
    "Kampen": "BU0166"
  ,
    "Losser": "BU0168"
  ,
    "Noordoostpolder": "BU0171"
  ,
    "Oldenzaal": "BU0173"
  ,
    "Ommen": "BU0175"
  ,
    "Raalte": "BU0177"
  ,
    "Staphorst": "BU0180"
  ,
    "Tubbergen": "BU0183"
  ,
    "Urk": "BU0184"
  ,
    "Wierden": "BU0189"
  ,
    "Zwolle": "BU0193"
  ,
    "Aalten": "BU0197"
  ,
    "Apeldoorn": "BU0200"
  ,
    "Arnhem": "BU0202"
  ,
    "Barneveld": "BU0203"
  ,
    "Beuningen": "BU0209"
  ,
    "Brummen": "BU0213"
  ,
    "Buren": "BU0214"
  ,
    "Culemborg": "BU0216"
  ,
    "Doesburg": "BU0221"
  ,
    "Doetinchem": "BU0222"
  ,
    "Druten": "BU0225"
  ,
    "Duiven": "BU0226"
  ,
    "Ede": "BU0228"
  ,
    "Elburg": "BU0230"
  ,
    "Epe": "BU0232"
  ,
    "Ermelo": "BU0233"
  ,
    "Geldermalsen": "BU0236"
  ,
    "Harderwijk": "BU0243"
  ,
    "Hattem": "BU0244"
  ,
    "Heerde": "BU0246"
  ,
    "Heumen": "BU0252"
  ,
    "Lochem": "BU0262"
  ,
    "Maasdriel": "BU0263"
  ,
    "Nijkerk": "BU0267"
  ,
    "Nijmegen": "BU0268"
  ,
    "Oldebroek": "BU0269"
  ,
    "Putten": "BU0273"
  ,
    "Renkum": "BU0274"
  ,
    "Rheden": "BU0275"
  ,
    "Rozendaal": "BU0277"
  ,
    "Scherpenzeel": "BU0279"
  ,
    "Tiel": "BU0281"
  ,
    "Voorst": "BU0285"
  ,
    "Wageningen": "BU0289"
  ,
    "Westervoort": "BU0293"
  ,
    "Winterswijk": "BU0294"
  ,
    "Wijchen": "BU0296"
  ,
    "Zaltbommel": "BU0297"
  ,
    "Zevenaar": "BU0299"
  ,
    "Zutphen": "BU0301"
  ,
    "Nunspeet": "BU0302"
  ,
    "Dronten": "BU0303"
  ,
    "Neerijnen": "BU0304"
  ,
    "Amersfoort": "BU0307"
  ,
    "Baarn": "BU0308"
  ,
    "De Bilt": "BU0310"
  ,
    "Bunnik": "BU0312"
  ,
    "Bunschoten": "BU0313"
  ,
    "Eemnes": "BU0317"
  ,
    "Houten": "BU0321"
  ,
    "Leusden": "BU0327"
  ,
    "Lopik": "BU0331"
  ,
    "Montfoort": "BU0335"
  ,
    "Renswoude": "BU0339"
  ,
    "Rhenen": "BU0340"
  ,
    "Soest": "BU0342"
  ,
    "Utrecht": "BU0344"
  ,
    "Veenendaal": "BU0345"
  ,
    "Woudenberg": "BU0351"
  ,
    "Wijk bij Duurstede": "BU0352"
  ,
    "IJsselstein": "BU0353"
  ,
    "Zeist": "BU0355"
  ,
    "Nieuwegein": "BU0356"
  ,
    "Aalsmeer": "BU0358"
  ,
    "Alkmaar": "BU0361"
  ,
    "Amstelveen": "BU0362"
  ,
    "Amsterdam": "BU0363"
  ,
    "Beemster": "BU0370"
  ,
    "Bergen (NH.)": "BU0373"
  ,
    "Beverwijk": "BU0375"
  ,
    "Blaricum": "BU0376"
  ,
    "Bloemendaal": "BU0377"
  ,
    "Castricum": "BU0383"
  ,
    "Diemen": "BU0384"
  ,
    "Edam-Volendam": "BU0385"
  ,
    "Enkhuizen": "BU0388"
  ,
    "Haarlem": "BU0392"
  ,
    "Haarlemmerliede en Spaarnwoude": "BU0393"
  ,
    "Haarlemmermeer": "BU0394"
  ,
    "Heemskerk": "BU0396"
  ,
    "Heemstede": "BU0397"
  ,
    "Heerhugowaard": "BU0398"
  ,
    "Heiloo": "BU0399"
  ,
    "Den Helder": "BU0400"
  ,
    "Hilversum": "BU0402"
  ,
    "Hoorn": "BU0405"
  ,
    "Huizen": "BU0406"
  ,
    "Landsmeer": "BU0415"
  ,
    "Langedijk": "BU0416"
  ,
    "Laren": "BU0417"
  ,
    "Medemblik": "BU0420"
  ,
    "Oostzaan": "BU0431"
  ,
    "Opmeer": "BU0432"
  ,
    "Ouder-Amstel": "BU0437"
  ,
    "Purmerend": "BU0439"
  ,
    "Schagen": "BU0441"
  ,
    "Texel": "BU0448"
  ,
    "Uitgeest": "BU0450"
  ,
    "Uithoorn": "BU0451"
  ,
    "Velsen": "BU0453"
  ,
    "Weesp": "BU0457"
  ,
    "Zandvoort": "BU0473"
  ,
    "Zaanstad": "BU0479"
  ,
    "Alblasserdam": "BU0482"
  ,
    "Alphen aan den Rijn": "BU0484"
  ,
    "Barendrecht": "BU0489"
  ,
    "Drechterland": "BU0498"
  ,
    "Brielle": "BU0501"
  ,
    "Capelle aan den IJssel": "BU0502"
  ,
    "Delft": "BU0503"
  ,
    "Dordrecht": "BU0505"
  ,
    "Gorinchem": "BU0512"
  ,
    "Gouda": "BU0513"
  ,
    "Den Haag": "BU0518"
  ,
    "Hardinxveld-Giessendam": "BU0523"
  ,
    "Hellevoetsluis": "BU0530"
  ,
    "Hendrik-Ido-Ambacht": "BU0531"
  ,
    "Stede Broec": "BU0532"
  ,
    "Hillegom": "BU0534"
  ,
    "Katwijk": "BU0537"
  ,
    "Krimpen aan den IJssel": "BU0542"
  ,
    "Leerdam": "BU0545"
  ,
    "Leiden": "BU0546"
  ,
    "Leiderdorp": "BU0547"
  ,
    "Lisse": "BU0553"
  ,
    "Maassluis": "BU0556"
  ,
    "Nieuwkoop": "BU0569"
  ,
    "Noordwijk": "BU0575"
  ,
    "Noordwijkerhout": "BU0576"
  ,
    "Oegstgeest": "BU0579"
  ,
    "Oud-Beijerland": "BU0584"
  ,
    "Binnenmaas": "BU0585"
  ,
    "Korendijk": "BU0588"
  ,
    "Oudewater": "BU0589"
  ,
    "Papendrecht": "BU0590"
  ,
    "Ridderkerk": "BU0597"
  ,
    "Rotterdam": "BU0599"
  ,
    "Rijswijk": "BU0603"
  ,
    "Schiedam": "BU0606"
  ,
    "Sliedrecht": "BU0610"
  ,
    "Cromstrijen": "BU0611"
  ,
    "Albrandswaard": "BU0613"
  ,
    "Westvoorne": "BU0614"
  ,
    "Strijen": "BU0617"
  ,
    "Vianen": "BU0620"
  ,
    "Vlaardingen": "BU0622"
  ,
    "Voorschoten": "BU0626"
  ,
    "Waddinxveen": "BU0627"
  ,
    "Wassenaar": "BU0629"
  ,
    "Woerden": "BU0632"
  ,
    "Zoetermeer": "BU0637"
  ,
    "Zoeterwoude": "BU0638"
  ,
    "Zwijndrecht": "BU0642"
  ,
    "Borsele": "BU0654"
  ,
    "Goes": "BU0664"
  ,
    "West Maas en Waal": "BU0668"
  ,
    "Hulst": "BU0677"
  ,
    "Kapelle": "BU0678"
  ,
    "Middelburg": "BU0687"
  ,
    "Giessenlanden": "BU0689"
  ,
    "Reimerswaal": "BU0703"
  ,
    "Zederik": "BU0707"
  ,
    "Terneuzen": "BU0715"
  ,
    "Tholen": "BU0716"
  ,
    "Veere": "BU0717"
  ,
    "Vlissingen": "BU0718"
  ,
    "Lingewaal": "BU0733"
  ,
    "De Ronde Venen": "BU0736"
  ,
    "Tytsjerksteradiel": "BU0737"
  ,
    "Aalburg": "BU0738"
  ,
    "Asten": "BU0743"
  ,
    "Baarle-Nassau": "BU0744"
  ,
    "Bergen op Zoom": "BU0748"
  ,
    "Best": "BU0753"
  ,
    "Boekel": "BU0755"
  ,
    "Boxmeer": "BU0756"
  ,
    "Boxtel": "BU0757"
  ,
    "Breda": "BU0758"
  ,
    "Deurne": "BU0762"
  ,
    "Pekela": "BU0765"
  ,
    "Dongen": "BU0766"
  ,
    "Eersel": "BU0770"
  ,
    "Eindhoven": "BU0772"
  ,
    "Etten-Leur": "BU0777"
  ,
    "Geertruidenberg": "BU0779"
  ,
    "Gilze en Rijen": "BU0784"
  ,
    "Goirle": "BU0785"
  ,
    "Grave": "BU0786"
  ,
    "Haaren": "BU0788"
  ,
    "Helmond": "BU0794"
  ,
    "'s-Hertogenbosch": "BU0796"
  ,
    "Heusden": "BU0797"
  ,
    "Hilvarenbeek": "BU0798"
  ,
    "Loon op Zand": "BU0809"
  ,
    "Mill en Sint Hubert": "BU0815"
  ,
    "Nuenen, Gerwen en Nederwetten": "BU0820"
  ,
    "Oirschot": "BU0823"
  ,
    "Oisterwijk": "BU0824"
  ,
    "Oosterhout": "BU0826"
  ,
    "Oss": "BU0828"
  ,
    "Rucphen": "BU0840"
  ,
    "Sint-Michielsgestel": "BU0845"
  ,
    "Someren": "BU0847"
  ,
    "Son en Breugel": "BU0848"
  ,
    "Steenbergen": "BU0851"
  ,
    "Waterland": "BU0852"
  ,
    "Tilburg": "BU0855"
  ,
    "Uden": "BU0856"
  ,
    "Valkenswaard": "BU0858"
  ,
    "Veldhoven": "BU0861"
  ,
    "Vught": "BU0865"
  ,
    "Waalre": "BU0866"
  ,
    "Waalwijk": "BU0867"
  ,
    "Werkendam": "BU0870"
  ,
    "Woensdrecht": "BU0873"
  ,
    "Woudrichem": "BU0874"
  ,
    "Zundert": "BU0879"
  ,
    "Wormerland": "BU0880"
  ,
    "Onderbanken": "BU0881"
  ,
    "Landgraaf": "BU0882"
  ,
    "Beek": "BU0888"
  ,
    "Beesel": "BU0889"
  ,
    "Bergen (L.)": "BU0893"
  ,
    "Brunssum": "BU0899"
  ,
    "Gennep": "BU0907"
  ,
    "Heerlen": "BU0917"
  ,
    "Kerkrade": "BU0928"
  ,
    "Maastricht": "BU0935"
  ,
    "Meerssen": "BU0938"
  ,
    "Mook en Middelaar": "BU0944"
  ,
    "Nederweert": "BU0946"
  ,
    "Nuth": "BU0951"
  ,
    "Roermond": "BU0957"
  ,
    "Schinnen": "BU0962"
  ,
    "Simpelveld": "BU0965"
  ,
    "Stein": "BU0971"
  ,
    "Vaals": "BU0981"
  ,
    "Venlo": "BU0983"
  ,
    "Venray": "BU0984"
  ,
    "Voerendaal": "BU0986"
  ,
    "Weert": "BU0988"
  ,
    "Valkenburg aan de Geul": "BU0994"
  ,
    "Lelystad": "BU0995"
  ,
    "Horst aan de Maas": "BU1507"
  ,
    "Oude IJsselstreek": "BU1509"
  ,
    "Teylingen": "BU1525"
  ,
    "Utrechtse Heuvelrug": "BU1581"
  ,
    "Oost Gelre": "BU1586"
  ,
    "Koggenland": "BU1598"
  ,
    "Lansingerland": "BU1621"
  ,
    "Leudal": "BU1640"
  ,
    "Maasgouw": "BU1641"
  ,
    "Eemsmond": "BU1651"
  ,
    "Gemert-Bakel": "BU1652"
  ,
    "Halderberge": "BU1655"
  ,
    "Heeze-Leende": "BU1658"
  ,
    "Laarbeek": "BU1659"
  ,
    "De Marne": "BU1663"
  ,
    "Reusel-De Mierden": "BU1667"
  ,
    "Roerdalen": "BU1669"
  ,
    "Roosendaal": "BU1674"
  ,
    "Schouwen-Duiveland": "BU1676"
  ,
    "Aa en Hunze": "BU1680"
  ,
    "Borger-Odoorn": "BU1681"
  ,
    "Cuijk": "BU1684"
  ,
    "Landerd": "BU1685"
  ,
    "De Wolden": "BU1690"
  ,
    "Noord-Beveland": "BU1695"
  ,
    "Wijdemeren": "BU1696"
  ,
    "Noordenveld": "BU1699"
  ,
    "Twenterand": "BU1700"
  ,
    "Westerveld": "BU1701"
  ,
    "Sint Anthonis": "BU1702"
  ,
    "Lingewaard": "BU1705"
  ,
    "Cranendonck": "BU1706"
  ,
    "Steenwijkerland": "BU1708"
  ,
    "Moerdijk": "BU1709"
  ,
    "Echt-Susteren": "BU1711"
  ,
    "Sluis": "BU1714"
  ,
    "Drimmelen": "BU1719"
  ,
    "Bernheze": "BU1721"
  ,
    "Ferwerderadiel": "BU1722"
  ,
    "Alphen-Chaam": "BU1723"
  ,
    "Bergeijk": "BU1724"
  ,
    "Bladel": "BU1728"
  ,
    "Gulpen-Wittem": "BU1729"
  ,
    "Tynaarlo": "BU1730"
  ,
    "Midden-Drenthe": "BU1731"
  ,
    "Overbetuwe": "BU1734"
  ,
    "Hof van Twente": "BU1735"
  ,
    "Neder-Betuwe": "BU1740"
  ,
    "Rijssen-Holten": "BU1742"
  ,
    "Geldrop-Mierlo": "BU1771"
  ,
    "Olst-Wijhe": "BU1773"
  ,
    "Dinkelland": "BU1774"
  ,
    "Westland": "BU1783"
  ,
    "Midden-Delfland": "BU1842"
  ,
    "Berkelland": "BU1859"
  ,
    "Bronckhorst": "BU1876"
  ,
    "Sittard-Geleen": "BU1883"
  ,
    "Kaag en Braassem": "BU1884"
  ,
    "Dantumadiel": "BU1891"
  ,
    "Zuidplas": "BU1892"
  ,
    "Peel en Maas": "BU1894"
  ,
    "Oldambt": "BU1895"
  ,
    "Zwartewaterland": "BU1896"
  ,
    "Súdwest-Fryslân": "BU1900"
  ,
    "Bodegraven-Reeuwijk": "BU1901"
  ,
    "Eijsden-Margraten": "BU1903"
  ,
    "Stichtse Vecht": "BU1904"
  ,
    "Hollands Kroon": "BU1911"
  ,
    "Leidschendam-Voorburg": "BU1916"
  ,
    "Goeree-Overflakkee": "BU1924"
  ,
    "Pijnacker-Nootdorp": "BU1926"
  ,
    "Molenwaard": "BU1927"
  ,
    "Nissewaard": "BU1930"
  ,
    "Krimpenerwaard": "BU1931"
  ,
    "De Fryske Marren": "BU1940"
  ,
    "Gooise Meren": "BU1942"
  ,
    "Berg en Dal": "BU1945"
  ,
    "Meierijstad": "BU1948"
  ,
    "Waadhoeke": "BU1949"
  ,
    "Westerwolde": "BU1950"
  ,
    "Midden-Groningen": "BU1952"
  ,
    "Montferland": "BU1955"
}