// Make Mapbox style Expression syntax from stop definition
function makeStyleSyntax(stops, exp, prop) {  // Color stops, expressionType, Property
  console.log("makeStyle")
  // array of arrays to one array list
  let rv = [];
  stops.forEach(function (item) {
    rv.push(item[0], item[1])
  });
  let colors;
  if (exp === "interpolate") {
    colors = ["interpolate", ["linear"], ["to-number", ["get", prop]]].concat(rv);
  } else {
    colors = [exp, ["get", prop]].concat(rv, "#000000");
  };
  return colors
}

// Make color scales with D3ranges
function makeColorScale(rangeStops, c) {
  console.log("makeColors")

  let stops = [];
  let rangeStep = 1 / rangeStops.length;
  let inter = 0;
  let color = "";
  if (rangeStops[0] === 0 || rangeStops[0] === 1005) {
    stops.push([rangeStops[0], "#000000"])
    rangeStops.shift();
  }

  for (let i = 0; i < rangeStops.length; i++) {


    if (c === "blue") {
      color = d3.interpolateCividis(1 - inter)
    }
    else if (c === "red") {
      color = d3.interpolateCividis(1 - inter)
    }
    else if (c === "electra") {
      color = d3.interpolateRdYlGn(1 - inter)
    }

    else if (c === "gas") {
      color = d3.interpolateRdYlGn(1 - inter)
    }
    else if (c === "perc") {
      color = d3.interpolateRdYlGn(inter)
    }
    else {
      color = d3.interpolateMagma(inter)
    }
    inter = inter + rangeStep;
    stops.push([rangeStops[i], color])
  }
  return stops
}

// Make legenda from stops
var updateLegenda = function (stops) {
  console.log("updateLegenda")

  $('#legenda').empty();

  $.each(stops, function (index) {
    item = stops[index];
    value = item[0];
    color = item[1];
    var legenda_item = '<div class="keychip" style="border-color:' + color + '">' + value + "</div>";
    $('#legenda').append(legenda_item);
  });
};


// Default bouwjaar stops to start with.
let beginStopsRangeBAG = [1005, 1750, 1800, 1850, 1900, 1930, 1945, 1960, 1975, 1985, 1990, 1995, 2000, 2010, 2020]
let beginStopsBAG = makeColorScale(beginStopsRangeBAG);
let colorsBAG = makeStyleSyntax(beginStopsBAG, "interpolate", 'bouwjaar');

// Defualt Buurten stops to start with 
let cbs_gasRange = [
  0, 100, 500000, 1000000, 1300250, 1800500, 1900750, 2000000, 3000250, 3500500, 4000750, 4500000, 5000500, 6000000, 7000500, 7500000, 8000500, 11000000];
let cbs_gas_stops = makeColorScale(cbs_gasRange, "blue");
let cbs_gas_colors = makeStyleSyntax(cbs_gas_stops, "interpolate", "totale_aardgaslevering_woningen");


var style_object_buurten = {
  "version": 8,
  "name": "cbsbuurt",
  "glyphs": "https://geodata.nationaalgeoregister.nl/beta/topotiles-viewer/font/{fontstack}/{range}.pbf",
  "sources": {
    "buurten": {
      "type": "vector",
      "tiles": [
        window._env_.TEGOLA_URL + "maps/cbs/{z}/{x}/{y}.vector.pbf?debug=false"
      ]
    },
    "bag3d": {
      "type": "vector",
      "tiles": [
        window._env_.TEGOLA_URL + "maps/bag3d/{z}/{x}/{y}.vector.pbf?debug=false"
      ]
    },
    "pdok": {
      "type": "vector",
      "url": "https://geodata.nationaalgeoregister.nl/beta/topotiles-viewer/styles/tilejson.json"
    }
  },
  "layers": [
    {
      "id": "background",
      "type": "background",
      "paint": {
        "background-color": "#0f0f0f"
      }
    },
    {
      "id": "water",
      "type": "fill",
      "source": "pdok",
      "source-layer": "water",
      "paint": {
        "fill-color": "#000000",
        "fill-outline-color": "#000000"
      }
    },
    {
      "id": "buurten",
      "type": "fill",
      "source": "buurten",
      "source-layer": "buurten",
      "maxzoom": 11,
      "minzoom": 0,
      "paint": {
        "fill-color": cbs_gas_colors
      }
    },
    {
      "id": "buurt_borders",
      "type": "line",
      "source": "buurten",
      "source-layer": "buurten",
      "paint": {
        'line-width': ["interpolate", ["linear"], ["zoom"],
          0, 0,
          8, 0.1,
          11, 0.5,
          16, 4
        ],
        'line-color': "rgb(80, 80, 80)"
      }
    },
    {
      "id": "bag3d",
      "type": "fill-extrusion",
      "source": "bag3d",
      "source-layer": "bag3d",
      "maxzoom": 17,
      "minzoom": 11,
      "paint": {
        "fill-extrusion-base": 0,
        "fill-extrusion-height": ["interpolate", ["linear"], ["zoom"],
          15, 0,
          16, ["*", 1.05, ["get", "roof-0.75"]]
        ],
        "fill-extrusion-color": colorsBAG
      }
    },
    {
      "id": "bag3d-outline",
      "type": "line",
      "source": "bag3d",
      "source-layer": "bag3d",
      "maxzoom": 17,
      "minzoom": 11,
      "paint": {
        "line-color": "#0f0f0f",
        "line-width": ["interpolate", ["linear"], ["zoom"],
          11, 0.5,
          15.5, 0
        ]
      }
    },
    {
      "id": "labels",
      "type": "symbol",
      "source": "pdok",
      "source-layer": "label",
      "maxzoom": 20,
      "minzoom": 5,
      "filter": [">=", "z_index", 1000],
      "layout": {
        "visibility": "visible",
        "symbol-placement": "point",
        "symbol-avoid-edges": false,
        "text-field": "{name}",
        "text-font": ["Rijksoverheid Sans Text TT Bold_2_0"],
        "text-size": 20,
        "text-max-width": 5,
        "text-anchor": "center",
        "text-line-height": 1,
        "text-justify": "center",
        "text-padding": 20,
        "text-allow-overlap": false
      },
      "paint": {
        "text-opacity": 1,
        "text-color": "#0f0f0f",
        "text-halo-color": "#ffffff",
        "text-halo-width": 2
      }
    }
  ]
};


//Initialize Map
var map = new mapboxgl.Map({
  container: "map",
  hash: true,
  style: style_object_buurten,
  zoom: 14,
  maxZoom: 16.5,
  minZoom: 6,
  pitch: 60,
  bearing: 60,
  center: [4.88, 52.35]
});


// CONTROLS
scale = new mapboxgl.ScaleControl({
  maxWidth: 180,
  unit: 'metric'
});
map.addControl(scale);

var nav = new mapboxgl.NavigationControl();
map.addControl(nav, 'top-left');

var Draw = new MapboxDraw();

map.addControl(Draw, 'top-left');

map.addControl(new mapboxgl.AttributionControl({
  compact: true
}));

// Map POPUP
// let prop = "bouwjaar";
// map.on('click', 'bag3d', function (e) {
//   console.log(e.features[0]);
//   new mapboxgl.Popup()
//     .setLngLat(e.lngLat)
//     .setHTML(e.features[0].properties)
//     .addTo(map);
// });

// MAP popup with all properties!
map.on('click', 'bag3d', function (e) {
  var feature = e.features[0];
  var text = '';
  text = text + `<h3>Layer ${feature.sourceLayer}</h3>`;
  for (const key in feature.properties) {
    text = text + `<tr><td><b> ${key}: </b> </td><td>  ${feature.properties[key]} </td></tr>`
  }
  text = '<table>' + text + '</table>'
  new mapboxgl.Popup()
    .setLngLat(e.lngLat)
    .setHTML(text)
    .addTo(map);
});

// Mouse over pointer cursor
map.on('mousemove', function (e) {
  let features = map.queryRenderedFeatures(e.point, { layers: ['bag3d'] })
  map.getCanvas().style.cursor = features.length ? "pointer" : "";
});

// INIT values 
// legenda = document.getElementById('legenda');
let stops_range = beginStopsRangeBAG;
let stops = beginStopsBAG;
let style_attribute = "boujaar";
let slider = document.getElementById('slider');
let sliderValue = document.getElementById('slider-value');

// Switch to neighborhoods ipv buildigns at zoom 
let zoomThreshold = 11
$(document).ready(function () {
  console.log('ready');
  updateLegenda(stops);
  map.on('zoom', function () {
    if (map.getZoom() <= zoomThreshold) {
      updateLegenda(cbs_gas_stops);
    } else {
      updateLegenda(stops);
    }
  });
});


// Building age button
$('#swap_age').on('click', function () {
  console.log('swap_age');
  stops = makeColorScale(beginStopsRangeBAG);
  prop = 'bouwjaar';
  let colors = makeStyleSyntax(stops, "interpolate", prop);
  map.setPaintProperty('bag3d', "fill-extrusion-color", colors);
  updateLegenda(stops);
});


// Gas dropdown
var selectgas = function () {
  stops_range = [
    0, 50, 100, 440, 855, 1190, 1220, 1330, 1440, 1550, 1660, 1870, 1980, 2190, 2200, 2510, 3020, 10000];
  stops = makeColorScale(stops_range, "blue");
  var year = $('#year_gasm3 option:selected').val()
  prop = 'gasm3_' + year;
  let colors = makeStyleSyntax(stops, "interpolate", prop);
  map.setPaintProperty('bag3d', "fill-extrusion-color", colors);
  updateLegenda(stops);
}

$('#year_gasm3').on('change focus', function () {
  console.log('swap_gasm3');
  selectgas()
});

// Electra dropdown
var selectkwh = function () {
  stops_range = [
    0, 1000, 2000, 2200, 2500, 2700, 2990, 3220, 3330, 3440, 3550, 3660, 3870, 3980, 4190, 5200, 6510, 9020, 10000, 40000]
  stops = makeColorScale(stops_range, "red");
  var year = $('#year_kwh option:selected').val()
  prop = 'kwh_' + year;
  let colors = makeStyleSyntax(stops, "interpolate", prop);
  map.setPaintProperty('bag3d', "fill-extrusion-color", colors);
  console.log('swap_kwh ' + year);
  updateLegenda(stops);
}

$('#year_kwh').on('change focus', function () {
  selectkwh()
});


// TRENDS
trend_stops = [
  -5000,
  -2000,
  -1000,
  -100,
  -75,
  -50,
  -30,
  -10,
  0,
  10,
  30,
  50,
  75,
  100,
  1000,
  2000,
  5000
]

percentage_stops = [
  -100,
  -20,
  -10,
  -7.5,
  -5,
  -2,
  0,
  2,
  5,
  7.5,
  10,
  20,
  100
]


zero_one_stops = [
  0,
  0.10,
  0.20,
  0.22,
  0.25,
  0.27,
  0.29,
  0.32,
  0.33,
  0.34,
  0.35,
  0.36,
  0.38,
  0.39,
  0.41,
  0.52,
  0.65,
  0.90,
  1
]


legenda_types = {
  'gasm3_trend': trend_stops,
  'gasm3_trend_p': percentage_stops,
  'gasm3_covariance': zero_one_stops,
  'gasm3_r2': zero_one_stops,
  'kwh_trend': trend_stops,
  'kwh_trend_p': percentage_stops,
  'kwh_covariance': zero_one_stops,
  'kwh_r2': zero_one_stops,
}



// Dropdown gas trends
$('#datascience_gas').on('change focus', function () {
  var attribute = $('#datascience_gas option:selected').val()
  console.log(attribute)
  stops = makeColorScale(legenda_types[attribute], "gas");
  prop = attribute;
  let colors = makeStyleSyntax(stops, "interpolate", prop);
  map.setPaintProperty('bag3d', "fill-extrusion-color", colors);
  updateLegenda(stops);
});

// Dropdown electra trends
$('#datascience_energie').on('change focus', function () {
  var attribute = $('#datascience_energie option:selected').val()
  stops = makeColorScale(legenda_types[attribute], "electra");
  prop = attribute;
  let colors = makeStyleSyntax(stops, "interpolate", prop);
  map.setPaintProperty('bag3d', "fill-extrusion-color", colors);
  updateLegenda(stops);
});


// ENERGIE LABELS 
$('#swap_elabel').on('click', function () {

  console.log('swap_elabel');

  style_attribute = 'elabel'
  prop = 'elabel';
  stops = [
    [0, "#000000"], // null
    [1, "#009342"], // A
    [2, "#1ba943"], // B
    [3, "#9ecf1b"], // C
    [4, "#f8f51c"], // D
    [5, "#f4b003"], // E
    [6, "#df6d14"], // F
    [7, "#db261d"]  // G
  ]

  let colors = makeStyleSyntax(stops, "match", prop)
  map.setPaintProperty('bag3d', 'fill-extrusion-color', colors);
  stops = [
    ["Geen data", "#000000"], // null
    ["A", "#009342"], // A
    ["B", "#1ba943"], // B
    ["C", "#9ecf1b"], // C
    ["D", "#f8f51c"], // D
    ["E", "#f4b003"], // E
    ["F", "#df6d14"], // F
    ["G", "#db261d"]  // G
  ]

  updateLegenda(stops);

});


/*
 * Style items around percentage of slider green.
 */
slider.addEventListener('input', function (e) {
  if (!map.loaded()) {
    //avoid overloading the rendering..
    return;
  }
  // creat a new style with green feel around selection.
  p = parseInt(e.target.value, 10) / 100
  console.log(p);
  stop_index = Math.round(stops.length * p);
  // set selected color to green in a new array
  new_stops = JSON.parse(JSON.stringify(stops));

  if (new_stops === undefined) {
    return;
  }

  if (stop_index > 1) {
    new_stops[stop_index - 1][1] = "rgb(200,205,100)";
  }

  new_stops[stop_index][1] = "rgb(255,255,0)";

  if (stop_index < stops.length - 1) {
    new_stops[stop_index + 1][1] = "rgb(200,205,100)";
  }

  selectedvalue = new_stops[stop_index][0]
  // Value indicator
  sliderValue.textContent = e.target.value + '%' + ' value ' + selectedvalue;
  console.group(selectedvalue);
  if (map.loaded()) {
    // map.setFilter('bag3d', [ "<=", ["get", prop],  selectedvalue]);
    map.setPaintProperty('bag3d', 'fill-extrusion-color', {
      "property": prop,
      "stops": new_stops,
    });

  }
  updateLegenda(new_stops);
});

