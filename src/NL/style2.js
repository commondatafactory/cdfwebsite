var style2 = {
  "version": 8,
  "name": "cbsbuurt",
  "sources": {
    "buurten":{
      "type": "vector",
      "tiles": [
    	"http://127.0.0.1:8081/maps/cbs/{z}/{x}/{y}.vector.pbf?debug=false"
      ]
    },
    "bag3d":{
      "type": "vector",
      "tiles": [
    	"http://127.0.0.1:8081/maps/bag3d/{z}/{x}/{y}.vector.pbf?debug=false"
      ]
    },

  },

  "layers":[
    {
      "id":  "background",
      "type": "background",
      "paint": {
        "background-color":"#000000"
      }
    },

    {
      "id": "buurten",
      "type": "fill-extrusion",
      "source": "buurten",
      "source-layer": "buurten",
      "maxzoom": 15,
      "minzoom": 0,
      "paint": {
        "fill-extrusion-base": 0,
        "fill-extrusion-height": {
        	"property": 'totale_aardgaslevering_woningen',
			"stops": [
        		[{zoom: 12, value: 1}, 1],
        		[{zoom: 12, value: 11000000}, 7000],
        		[{zoom: 14, value: 1}, 1],
        		[{zoom: 14, value: 11000000}, 4000]
			]
        },

        "fill-extrusion-color": {
        	"property": 'totale_aardgaslevering_woningen',
        	"stops": [
        		//[1,   "rgb(0,141,81)"],
        		[0,   "rgb(1,21, 41)"],
        		[500000,  "rgb(1,41, 81)"],
        		[1000000, "rgb(1,19,157)"],
        		[1300250, "rgb(4,37,212)"],
        		[1800500, "rgb(6,74,243)"],
        		[1900750, "rgb(13,149,233)"],
        		[2000000, "rgb(25,207,210)"],
        		[3000250, "rgb(130,253,206)"],
        		[3500500, "rgb(164,235,79)"],
        		[4000750, "rgb(255,255,128)"],
        		[4500000, "rgb(255,214,0)"],
        		[5000500, "rgb(254,173,91)"],
        		[6000000, "rgb(250,124,1)"],
        		[7000500, "rgb(254,0,0)"],
        		[7500000, "rgb(129,0,65)"],
        		[8000500, "rgb(65,0,64)"],
        		[11000000, "rgb(35,0,34)"]
        	]
        },

        "fill-extrusion-opacity": {
        	"base": 1,
        	"stops": [[12,1],[12.5,1],[14,1]]
    	}
      }
    },

    {
      "id": "bag3d",
      "type": "fill-extrusion",
      "source": "bag3d",
      "source-layer": "bag3d",
      "maxzoom": 20,
      "minzoom": 9,
      "paint": {
        "fill-extrusion-base": 0,
        "fill-extrusion-height": {
        	"property": 'roof-0.75',
		"stops": [
		   [{zoom: 12, value: 1}, 1],
		   [{zoom: 12, value: 100}, 100],
		   [{zoom: 14, value: 1}, 1],
		   [{zoom: 14, value: 100}, 100]
		]
        },

        "fill-extrusion-color": {
        	"property": 'bouwjaar',
        	"stops": [
        		//[1,   "rgb(0,141,81)"],
        		[0, "rgb(35,0,34)"],
        		[1700, "rgb(65,0,64)"],
        		[1750, "rgb(129,0,65)"],
        		[1800, "rgb(254,0,0)"],
        		[1840, "rgb(250,124,1)"],
        		[1855, "rgb(254,173,91)"],
        		[1890, "rgb(255,114,0)"],
        		[1920, "rgb(255,155,128)" ],
        		[1930, "rgb(164,135,79)"  ],
        		[1940, "rgb(104,122,134)" ],
        		[1950, "rgb(130,153,206)" ],
        		[1960, "rgb(25,107,210)"  ],
        		[1970, "rgb(13,149,233)"  ],
        		[1980, "rgb(6,274,243)"    ],
        		[1990, "rgb(4,247,212)"    ],
        		[2000, "rgb(1,230,157)"    ],
        		[2010, "rgb(1,101, 81)"    ],
        		[2020, "rgb(1,100, 41)"    ],
        	]
        },
        "fill-extrusion-opacity": {
        	"base": 1,
        	"stops": [[12,1],[12.5,0.8],[14,0.82]]
    	}
      }
    },
  ]
};

