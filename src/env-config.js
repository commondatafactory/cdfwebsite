window._env_ = {
  BASE_URL: "http://127.0.0.1:8088/",
  TEGOLA_URL: "http://127.0.0.1:8081/",
  MAPSERVER_URL: "http://127.0.0.1:8082/",
  BAG3D_URL: "http://127.0.0.1:8088/NL/#13.8/52.365/4.9/0/40",
  CBS3D_URL: "http://127.0.0.1:8088/NL/#8.06/51.941/5.273/18.4/42",
}
