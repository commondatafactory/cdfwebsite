#FROM node:10.16.0-alpine AS build
#
## Install required build tools
#RUN apk add --no-cache \
#    build-base \
#    python \
#    openssl \
#    make \
#    git
#
## Copy only package.json en yarn.lock to make the dependency fetching step optional.
#COPY package.json \
#     package-lock.json \
#     /app/
#
#WORKDIR /app
#RUN npm install
#
## Build docs.
#COPY . /app
#WORKDIR /app
#RUN npm run build

# Copy static docs to alpine-based nginx container.
FROM nginx:alpine

# Add bash
RUN apk add --no-cache bash

# Copy nginx configuration
COPY docker/default.conf /etc/nginx/conf.d/default.conf
COPY docker/nginx.conf /etc/nginx/nginx.conf

COPY src /usr/share/nginx/html

# Add non-privileged user
RUN adduser -D -u 1001 appuser

# Set ownership nginx.pid and cache folder in order to run nginx as non-root user
RUN touch /var/run/nginx.pid && \
    chown -R appuser /var/run/nginx.pid && \
    chown -R appuser /var/cache/nginx && \
    chown -R appuser /usr/share/nginx/html/NL && \
    chown -R appuser /usr/share/nginx/html/

# Copy .env file and shell script to container
WORKDIR /usr/share/nginx/html
COPY ./env.sh .
COPY ./replace.sh .
COPY .env .

# Make our shell script executable
RUN chmod +x env.sh

USER appuser

# Start Nginx server
CMD ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
